from __future__ import print_function
import base64
import os.path
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
from lxml import etree
from typing import List
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.errors import HttpError

# If modifying these scoes, delete the file token.json
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']


def get_report() -> List[str]:
    """
    Shows basic usage of the Gmail API,
    List the user's Gmail labels
    :return:
    """

    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '../credentials.json',
                SCOPES
            )
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    try:
        # Call the Gmail API
        service = build('gmail', 'v1', credentials=creds)
        resulst_labels = service.users().labels().list(userId='me').execute()
        results_messages = service.users().messages().list(userId='me', maxResults='2', labelIds=['Label_354112651981415264']).execute()
        messages = results_messages.get('messages', []) # get messages from label selected
        labels = resulst_labels.get('labels', []) # get labels on gmail, Label_354112651981415264 belongs to Multilineal, in that place arrives the reportes

        for label in messages:
            email = service.users().messages().get(userId='me', id=label['id'], format='full').execute()
            body = email['payload']['parts'][1]['body']
           #if 'data' in body.keys(): just in case where your reports dont arrive on a label where only threre reports
           #   msg_str = base64.urlsafe_b64decode(body['data'])
           #    #link = re.findall(r"(?:href=\")[h][t][t][p].*(?:xlsx)", str(msg_str))
           #    #location = link[0][6:]
           #    tree = etree.parse(msg_str.decode())
           #    link = tree.xpath("//td/a/@href")
           #    print(link)
            msg_str = base64.urlsafe_b64decode(body['data']) # decode the body, if not the result will be a base64 string
            soup = BeautifulSoup(msg_str, "html.parser")  # create a soup with msg_str
            tree = etree.HTML(str(soup)) # get a HTML tree with the soup
            link = tree.xpath("//td/a/@href")[0] # get link for the report
            filename = tree.xpath("//div[@class='gmail_attr']/text()[preceding-sibling::br][4]")[0].split(" ")[1].strip() # get filename of report 
            return [filename, link]

    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API
        print(f'An error occurred: {error}')


if __name__ == '__main__':
    print(get_report())
