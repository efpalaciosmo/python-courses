import subprocess

def runcmd(cmd, verbose= False, *args, **kwargs):
    process = subprocess.Popen(
            cmd,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE,
            text = True,
            shell = True
            )
    std_out, std_err = process.communicate()
    if verbose:
        print(std_out.strip(), std_err)
    pass 



link = "https://www.amazon.es/gp/f.html?C=1T1TYGCO7YHGD&M=urn:rtn:msg:2022122102351189fe5a46ca6849e4a153180c73a0p0eu&R=320CO0BC5RL2S&T=C&U=https%3A%2F%2Fcorvo-reports.s3.amazonaws.com%2FTRESAH%2F2022-12-21%2Fb8a2e2e1-981d-49fd-bb56-1e88bf2c1f61%25402022-12-21%252002%253A35%253A00.0%2FSP_Ubicaci%25C3%25B3n_RIB_ES_7dias.xlsx%3FX-Amz-Algorithm%3DAWS4-HMAC-SHA256%26X-Amz-Date%3D20221221T023511Z%26X-Amz-SignedHeaders%3Dhost%26X-Amz-Expires%3D604800%26X-Amz-Credential%3DAKIAY2R3XYZCX7FVBMWX%252F20221221%252Fus-east-1%252Fs3%252Faws4_request%26X-Amz-Signature%3D8684ef1ef3b858f59e4578aadaacb7cac6419a7c35bf0abd691857d00ff0805a&H=VRUOY4R2G9JF4TG8LVKINYGKHD8A"

runcmd("wget {}".format(link), verbose=True)
