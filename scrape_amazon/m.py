from bs4 import BeautifulSoup
import requests
from typing import List, Dict
import pandas as pd

# URL to where the scrape is done
URL = "https://www.amazon.com/s?k=playstation+5&sprefix=pla%2Caps%2C178&ref=nb_sb_ss_ts-doa-p_1_3"

# Headers for requests
HEADERS = (
    {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
        'Accept-Language': 'en-US, en; q=0.5'
    }
)


class Scraper:

    def __init__(self, URL: str, HEADERS: Dict[str, str]):
        self.page = requests.get(url=URL, headers=HEADERS)
        self.soup = BeautifulSoup(self.page.content, 'html.parser')

    def get_parent_info(self, tags_attrs: Dict[str,List[str]]) -> List[str]:
        data = []
        for tag, atributos in tags_attrs.items():
            dic = {atributos[0]: atributos[1]}
            print(self.soup.find(tag, attrs={'id': 'productTitle'}))
            result = self.soup.find(
                str(tag),
                attrs=dic
            )
            data.append(result.text.strip())

        return data


if __name__ == '__main__':
    scraper = Scraper(URL, HEADERS)
    tags_attrs = {
        'span': ['id', 'productTitle'],
        'td': ['class', 'a-size-base prodDetAttrValue']
    }
    infor = scraper.get_parent_info(tags_attrs)
    print(infor)