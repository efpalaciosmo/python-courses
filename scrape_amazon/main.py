from bs4 import BeautifulSoup
import requests
import pandas as pd

URL = "https://www.amazon.es/gp/bestsellers/lawn-garden/5940663031/"

# Headers for requests
HEADERS = (
    {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
        'Accept-Language': 'en-US, en; q=0.5'
    }
)

# get webpage info
webpage = requests.get(URL, headers=HEADERS)

# create a soup object containing all data
soup = BeautifulSoup(webpage.content, 'html.parser')

print(soup)

## fetch links as List of Tag Objects
#links = soup.find_all(
#    'a',
#    attrs={
#        'class': 'a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal'
#    }
#)
#
## build complete link
#product = 'https://amazon.com'+links[0].get('href')
#
## go to product page
#product_page = requests.get(url=product, headers=HEADERS)
#product_soup = BeautifulSoup(product_page.content, 'html.parser')
#
## get the title of the product
#title = product_soup.find(
#    'span',
#    attrs={
#        'id': 'productTitle'
#    }
#)
#price = product_soup.find(
#    'span',
#    attrs={
#        'class': 'a-price'
#    }
#).find(
#    'span',
#    attrs={
#        'class': 'a-offscreen'
#    }
#)
#asin = product_soup.find(
#    'td',
#    attrs={
#        'class': 'a-size-base prodDetAttrValue'
#    }
#)
