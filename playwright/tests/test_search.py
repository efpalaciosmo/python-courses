import pytest

from pages.result import DuckDuckGoResultPage
from pages.search import DuckDuckGoSearchPage
from playwright.sync_api import expect, Page

ANIMALS = [
    'panda',
    'python',
    'polar bear',
    'parrot',
    'panther'
]


@pytest.mark.parametrize('phrase', ANIMALS)  # just when you want to run parallel test
def test_basic_duckduckgo_search(
        phrase: str,
        page: Page,
        result_page: DuckDuckGoResultPage,
        search_page: DuckDuckGoSearchPage) -> None:

    # Given the DuckDuckGo home page is displayed
    search_page.load()

    # When the user searches for a phrase
    search_page.search(phrase)

    # Then the search result query is the phrase
    expect(result_page.search_input).to_have_value(phrase)
    ## Another way could be
    ## assert 'panda' == page.input_value('id=search_form_input_homepage')

    # And the search result links pertain to the phrase
    assert result_page.result_link_contains_phrase(phrase)

    # And the search result title contains the phrase
    expect(page).to_have_title(f'{phrase} at DuckDuckGo')