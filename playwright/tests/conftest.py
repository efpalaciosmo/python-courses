import pytest

from pages.result import DuckDuckGoResultPage
from pages.search import DuckDuckGoSearchPage
from pages.search_amazon import BestSellersPage
from pages.results_amazon import BestSellersResult
from playwright.sync_api import Page


@pytest.fixture()
def result_page(page: Page) -> DuckDuckGoResultPage:
    return DuckDuckGoResultPage(page)


@pytest.fixture()
def search_page(page: Page) -> DuckDuckGoSearchPage:
    return DuckDuckGoSearchPage(page)


@pytest.fixture()
def best_sellers(page: Page) -> BestSellersResult:
    return BestSellersResult(page)