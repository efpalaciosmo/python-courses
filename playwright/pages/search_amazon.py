from playwright.sync_api import Page


class BestSellersPage:
    URL = 'https://www.amazon.es/gp/bestsellers/lawn-garden/5940663031/'

    def __init__(self, page: Page) -> None:
        self.page = page

    def load(self) -> None:
        self.page.goto(self.URL)


BestSellersPage().load()
